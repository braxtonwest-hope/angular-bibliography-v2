import { Component, OnInit } from '@angular/core';
import { RepositoryService } from '../../services/repository.service';
import { Bibentry } from '../../models/bibentry.model';

import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-bibliography',
  templateUrl: './bibliography.component.html',
  styleUrls: ['./bibliography.component.css']
})
export class BibliographyComponent implements OnInit {

  private books : Bibentry[];

  constructor(private _repository : RepositoryService) { }

  ngOnInit() {
    this._repository.allBooks.subscribe (
      books => this.books = books
    );
  }

}
